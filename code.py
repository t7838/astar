import heapq

from Map import Map_Obj


class Node:
    """A representation of a node"""

    def __init__(self, parent, pos):
        self.parent = parent
        self.pos = pos

        self.g = 0
        self.h = 0
        self.f = 0

    def get_pos(self):
        return (self.pos[0], self.pos[1])

    def get_parent(self):
        return self.parent

    def __eq__(self, other):
        return self.pos == other.pos

    def __repr__(self):
        return f"{self.pos}"

    # defining less than for purposes of heap queue
    def __lt__(self, other):
        return self.f < other.f

    # defining greater than for purposes of heap queue
    def __gt__(self, other):
        return self.f > other.f


class A_star:

    def __init__(self):
        self.open = []
        self.closed = []
        self.paths = []

    # The main algorithm for A*
    def main(self, map,  start_pos, end_pos):

        # Create start- and end node
        start_node = Node(None, start_pos)
        end_node = Node(None, end_pos)

        # TODO: Er dette meningen å ha med?
        start_node.f = (end_node.get_pos()[
                        0]-start_node.get_pos()[0])**2 + (end_node.get_pos()[1]-start_node.get_pos()[1])**2

        # heapq.heapify(self.open)
        # heapq.heappush(self.open, start_node)
        self.open.append(start_node)

        # Loop until end is reached
        while len(self.open) > 0:

            # Get current node
            current_node = self.open[0]  # heapq.heappop(self.open)
            current_index = 0
            for index, item in enumerate(self.open):
                if item.f < current_node.f:
                    current_node = item
                    current_index = index

            # Pop the current node off the open list and add to the closed list
            self.open.pop(current_index)
            self.closed.append(current_node)

            # Found the goal
            if current_node == end_node:
                print("YASSS BRO")
                path = []
                current = current_node
                while current is not None:
                    path.append(current.pos)
                    current = current.parent
                return path[::-1]

            # Generate children
            adjacent_squares = ((-1, 0), (0, -1), (1, 0), (0, 1))
            children = []

            for new_pos in adjacent_squares:

                # Get node position
                node_pos = (
                    current_node.pos[0] + new_pos[0], current_node.pos[1] + new_pos[1])

                # Check if within bounds
                if node_pos[0] > (len(map) - 1) or node_pos[0] < 0 or node_pos[1] > (len(map[len(map) - 1]) - 1) or node_pos[1] < 0:
                    continue

                # Check if walkable terrain
                if not (map[node_pos[0]][node_pos[1]] in {1, 2, 3, 4}):
                    continue

                # Create new node and add to list of child nodes
                new_node = Node(current_node, node_pos)
                children.append(new_node)

            # Loop trough list of child nodes
            for child in children:

                # Child is on the closed list
                if len([closed_child for closed_child in self.closed if closed_child == child]) > 0:
                    continue

                # Calculate cost and heuristics
                child.g = current_node.g + \
                    map[child.pos[0]][child.pos[1]]

                # Manhattan distance
                child.h = (abs(child.pos[0] - end_node.pos[0]) +
                           abs(child.pos[1] - end_node.pos[1]))
                child.f = child.g + child.h

                # TODO: Hva skjer her?
                if len([open_node for open_node in self.open if child.pos == open_node.pos and child.g > open_node.g]) > 0:
                    continue

                # TODO: Velg enten heap eller list
                heapq.heappush(self.open, child)


def test():
    # Task to solve
    task = 4  # 1,2,3,4

    # Initialize objects and variables
    astar = A_star()
    m = Map_Obj(task)
    map = m.get_maps()[0]
    start = m.get_start_pos()
    end = m.get_end_goal_pos()

    # Find pah from start to end
    path = astar.main(map, start, end)

    # Colorization of the map
    path.pop()
    path.pop(0)
    for cordinate in path:
        m.str_map[cordinate[0], cordinate[1]] = "."

    # Show result
    print("Path:", path)
    m.show_map()


if __name__ == '__main__':
    test()
